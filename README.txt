Mass Email

Project Download URL - https://www.drupal.org/project/mass_email

Summary 
An interface to send mass email 
Admin URL - http://yoursites.com/admin/mass-email

Permissions
  Send Mass Email
  
Maintainer
Parvind Sharma, Passionate Stars
Twitter - https://twitter.com/passion8stars
website - http://www.passionatestars.com/
Email - contact@passionatestars.com
  
  
