<?php
function mass_email_form($form, &$form_state){
	global $user;
	
 $form['recipients'] = array(
    '#type' => 'textarea',
    '#title' => t('Recipients'),
    '#description' => t("You can copy/paste the multiple emails, enter one email per line. "),
    '#required' => TRUE,
  );
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
   $form['message'] = array(
    '#type' => 'text_format',
    '#title' => t('Message'),	
    '#required' => TRUE,
  );
   $form['from'] = array(
    '#type' => 'textfield',
    '#title' => t('From e-mail address'),
    '#maxlength' => 255,
    '#default_value' => $user->uid ? $user->mail : '',
    '#required' => TRUE,
  );
   $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Your name'),
    '#maxlength' => 255,
    '#default_value' => $user->uid ? format_username($user) : '',
    '#required' => TRUE,
  );  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}
/**
 * Form submission handler for mass_email_form().
 *
 */
function mass_email_form_submit($form, &$form_state) {
  global $user, $language;
  $values = $form_state['values'];
  $from = $values['from'];
  $params['subject'] = $values['subject'];
  $params['message'] = $values['message']['value'];
  $headers = array(
'MIME-Version' => '1.0',
'Content-Type' => 'text/html; charset=UTF-8; format=flowed',
'Content-Transfer-Encoding' => '8Bit',
'X-Mailer' => 'Drupal'
);
  $to_ar=explode(",",$values['recipients']); 
foreach($to_ar as $to){
	drupal_mail('mass_email',
	'send_mass_email',
	$to,
	language_default(),
	$params,
	$from,
	$headers
	);
}
  drupal_set_message(t('Your message has been sent.'));  
}
